package PasswordGenerator;

import java.util.*;

class ActionSize extends Action{
   private int size;

   public ActionSize(String message, int size){
      super(message);
      this.size = size;
   }

   public void act() throws Exception{
      System.out.println(MyView.sizePass + this.size);
      this.settings.setPassSize(this.size);
   }

}
