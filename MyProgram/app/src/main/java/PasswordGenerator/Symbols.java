package PasswordGenerator;

class Symbols{
   private String lowerCase = "abcdefghijklmnopqrstuvwxyz";
   private String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
   private String numbers = "0123456789";
   private String specSymbols = "!@#$%&*()_+-=[]|,./?><";

   public String getLowerCase(){
      return this.lowerCase;
   }

   public void setLowerCase(String lowerCase){
      this.lowerCase = lowerCase;
   }

   public String getUpperCase(){
      return this.upperCase;
   }

   public void setUpperCase(String upperCase){
      this.upperCase = upperCase;
   }

   public String getNumbers(){
      return this.numbers;
   }

   public void setNumbers(String numbers){
      this.numbers = numbers;
   }

   public String getSpecSymbols(){
      return this.specSymbols;
   }

   public void setSpecSymbols(String specSymbols){
      this.specSymbols = specSymbols;
   }
}
