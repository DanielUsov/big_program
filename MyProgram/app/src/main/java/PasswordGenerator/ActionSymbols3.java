package PasswordGenerator;

import java.util.*;

class ActionSymbols3 extends Action{

   public ActionSymbols3(String message){
      super(message);
   }

   public void act() throws Exception{
      this.settings.setNumber(!this.settings.getNumber());
      if (this.settings.getNumber()) {
         System.out.println(MyView.actionSymbols3Y);
      }
      else {
         System.out.println(MyView.actionSymbols3N);

      }
   }
}
