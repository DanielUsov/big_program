package PasswordGenerator;

class ActionSymbols4 extends Action {

   public ActionSymbols4(String message) {
      super(message);
   }

   public void act() throws Exception {
      this.settings.setSpec(!this.settings.getSpec());
      if (this.settings.getSpec()) {
         System.out.println(MyView.actionSymbols4Y);
      } else {
         System.out.println(MyView.actionSymbols4N);
      }
   }
}
