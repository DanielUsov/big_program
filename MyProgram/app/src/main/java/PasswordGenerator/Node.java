package PasswordGenerator;

import java.util.*;

class Node {
   public ArrayList<Action> action = new ArrayList<Action>();
   private Node parent;
   public ArrayList<Node> children = new ArrayList<Node>();

   public ArrayList<Action> getAction(){
      return this.action;
   }

   public void setAction(Action action){
      this.action.add(action);
   }

   public Node getParent(){
      return this.parent;
   }

   public void setParent(Node parent){
      this.parent = parent;
   }

   public ArrayList<Node> getChildren(){
      return this.children;
   }

   public void setChildren(Node children){
      this.children.add(children);
      children.parent = this;
   }

   public boolean hasParent(){
      return this.parent == null;
   }

   public int sizeChildren(){
      return this.children.size();
   }

   public int sizeAction(){
      return this.action.size();
   }


   public void output(){
      System.out.println(MyView.menuItems);
      if (hasParent()){
         System.out.println(MyView.exitInMenu);
      }
      else{
         System.out.println(MyView.backInMenu);
      }
      int num = 1;
      for (int i = 0; i < sizeChildren(); i++) {
         if (i == 0) {
            System.out.println(num + MyView.goToPassSize);
            num++;
         }
         if (i == 1) {
            System.out.println(num + MyView.goToPassSymbols);
            num++;
         }
      }
      for (int i = 0; i < sizeAction(); i++) {
         System.out.println(num + ": " + this.action.get(i).getMessage());
         num++;
      }
   }
}
