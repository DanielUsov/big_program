package PasswordGenerator;

import java.util.*;

class ActionSymbols1 extends Action{

   public ActionSymbols1(String message){
      super(message);
   }

   public void act() throws Exception{
      this.settings.setSmallaz(!this.settings.getSmallaz());
      if (this.settings.getSmallaz()) {
         System.out.println(MyView.actionSymbols1Y);
      }
      else {
         System.out.println(MyView.actionSymbols1N);

      }
   }
}
