package PasswordGenerator;

import java.util.*;

class ActionGeneratePass extends Action{

   public ActionGeneratePass(String message){
      super(message);
   }

   public void act() throws Exception{
      System.out.println(generate());
   }
   public String generate(){
      System.out.println(MyView.waiting);
      Symbols symbols = new Symbols();
      char[] password = new char[this.settings.getPassSize()];
      Random random = new Random();
      String combination = "";
      if (this.settings.getSmallaz()) {
         combination += symbols.getLowerCase();
      }
      if (this.settings.getBigAZ()) {
         combination += symbols.getUpperCase();
      }
      if (this.settings.getNumber()) {
         combination += symbols.getNumbers();
      }
      if (this.settings.getSpec()) {
         combination += symbols.getSpecSymbols();
      }
      for (int i = 0;i < password.length;i++ ) {
         password[i] = combination.charAt(random.nextInt(combination.length()));
      }
      return MyView.uPass + new String(password);
   }
}
