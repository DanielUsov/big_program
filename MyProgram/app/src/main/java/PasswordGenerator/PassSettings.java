package PasswordGenerator;

class PassSettings{
   public boolean smallaz;
   public boolean bigAZ;
   public boolean number;
   public boolean spec;
   public int passSize;

   public PassSettings(){
      this.smallaz = true;
      this.bigAZ = true;
      this.number = false;
      this.spec = false;
      this.passSize = 8;
   }

   public boolean getSmallaz(){
      return this.smallaz;
   }

   public void setSmallaz(boolean smallaz){
      this.smallaz = smallaz;
   }

   public boolean getBigAZ(){
      return this.bigAZ;
   }

   public void setBigAZ(boolean bigAZ){
      this.bigAZ = bigAZ;
   }

   public boolean getNumber(){
      return this.number;
   }

   public void setNumber(boolean number){
      this.number = number;
   }

   public boolean getSpec(){
      return this.spec;
   }

   public void setSpec(boolean spec){
      this.spec = spec;
   }

   public int getPassSize(){
      return this.passSize;

   }

   public void setPassSize(int passSize){
      this.passSize = passSize;
   }
}
