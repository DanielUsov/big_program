package PasswordGenerator;

abstract class Action{
   public String message;
   public static PassSettings settings = new PassSettings();

   public Action(String message){
      this.message = message;
   }

   public String getMessage(){
      return this.message;
   }

   abstract public void act() throws Exception;

}
