package PasswordGenerator;

import java.util.*;

class ActionSymbols2 extends Action{

   public ActionSymbols2(String message){
      super(message);
   }

   public void act() throws Exception{
      this.settings.setBigAZ(!this.settings.getBigAZ());
      if (this.settings.getBigAZ()) {
         System.out.println(MyView.actionSymbols2Y);
      }
      else {
         System.out.println(MyView.actionSymbols2N);

      }
   }
}
