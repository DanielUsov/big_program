package PasswordGenerator;

public class MyView{
   // MENU
   public static String actionGeneratePass = "Генерация вашего пароля";
   public static String actionSize1 = "Длина пароля: 8 (def)";
   public static String actionSize2 = "Длина пароля: 12";
   public static String actionSize3 = "Длина пароля: 14";
   public static String actionSize4 = "Длина пароля: 20";
   public static String actionSymbols1 = "Включить / выключить прописные буквы (def)";
   public static String actionSymbols2 = "Включить / выключить заглавные буквы (def)";
   public static String actionSymbols3 = "Включить / выключить цифры";
   public static String actionSymbols4 = "Включить / выключить специальные символы";

   // NODE
   public static String menuItems = "\nПункты меню: ";
   public static String exitInMenu = "0: Выйти";
   public static String backInMenu = "0: Вернуться на уровень назад";
   public static String goToPassSize = ": Настройки длины пароля";
   public static String goToPassSymbols = ": Настройки используемых символов";

   // ActionGeneratePass
   public static String waiting = "\nОжидайте, ваш пароль генерируется";
   public static String uPass = "\nВаш пароль: ";

   // ActionSize
   public static String sizePass = "Длина пароля: ";

   // ActionSymbols
   public static String itemActionSymbols =
   "1: Заглавные буквы (выбрано по умолчаниию) \n" +
   "2: Прописные буквы (выбрано по умолчаниию) \n" +
   "3: Цифры \n" +
   "4: Специальные символы \n";
   public static String actionSymbols1Y = "Прописные буквы включенны";
   public static String actionSymbols1N = "Прописные буквы отключены";
   public static String actionSymbols2Y = "Заглавные буквы включенны";
   public static String actionSymbols2N = "Заглавные буквы отключены";
   public static String actionSymbols3Y = "Цифры включенны";
   public static String actionSymbols3N = "Цифры отключены";
   public static String actionSymbols4Y = "Специальные символы включенны";
   public static String actionSymbols4N = "Специальные символы выключенны";


   // Exception
   public static String myExc= "\n Извините, но это явно не пункт меню, введите ещё раз \n";

}
