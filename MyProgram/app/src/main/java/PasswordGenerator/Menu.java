package PasswordGenerator;

import java.util.*;

class Menu {
   public static void main(String[] args) throws Exception {
      Scanner sc = new Scanner(System.in);
      int userInput = 0;
      Node start = new Node();
      Node level1 = new Node();
      Node level12 = new Node();
      Action one = new ActionGeneratePass(MyView.actionGeneratePass);
      start.setChildren(level1);
      start.setChildren(level12);
      start.setAction(one);
      level1.setAction(new ActionSize(MyView.actionSize1, 4));
      level1.setAction(new ActionSize(MyView.actionSize2, 8));
      level1.setAction(new ActionSize(MyView.actionSize3, 12));
      level1.setAction(new ActionSize(MyView.actionSize4, 20));
      level12.setAction(new ActionSymbols1(MyView.actionSymbols1));
      level12.setAction(new ActionSymbols2(MyView.actionSymbols2));
      level12.setAction(new ActionSymbols3(MyView.actionSymbols3));
      level12.setAction(new ActionSymbols4(MyView.actionSymbols4));

      while (true) {
         start.output();
         userInput = sc.nextInt();
         System.out.println();
         if (userInput == 0) {
            if (start.hasParent()) {
               sc.close();
               break;
            } else {
               start = start.getParent();
            }
         }
         if (userInput > start.sizeChildren() && userInput <= start.sizeAction() + start.sizeChildren()) {
            start.action.get(userInput - start.sizeChildren() - 1).act();
         }
         if (userInput > 0 && userInput <= start.sizeChildren()) {
            start = start.children.get(userInput - start.sizeChildren() + 1);
         }
      }

   }

}
