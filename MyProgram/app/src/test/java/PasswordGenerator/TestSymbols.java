package PasswordGenerator;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TestSymbols {
   @Test void testGetLowerCase() {
      String lowerCase = "abcdefghijklmnopqrstuvwxyz";
      Symbols a = new Symbols();
      assertEquals(lowerCase, a.getLowerCase());
   }

   @Test void testsetLowerCase() {
      String lowerCase = "abcdefghijklmnopqrstuvwxyzqq";
      Symbols a = new Symbols();
      a.setLowerCase("abcdefghijklmnopqrstuvwxyzqq");
      assertEquals(lowerCase, a.getLowerCase());
   }

   @Test void testGetUpperCase() {
      String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      Symbols a = new Symbols();
      assertEquals(upperCase, a.getUpperCase());
   }

   @Test void testSetUpperCase() {
      String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZaa";
      Symbols a = new Symbols();
      a.setUpperCase("ABCDEFGHIJKLMNOPQRSTUVWXYZaa");
      assertEquals(upperCase, a.getUpperCase());
   }

   @Test void testGetNumbers() {
      String numbers = "0123456789";
      Symbols a = new Symbols();
      assertEquals(numbers, a.getNumbers());
   }

   @Test void testSetNumbers(){
      String numbers = "0123456789000000";
      Symbols a = new Symbols();
      a.setNumbers("0123456789000000");
      assertEquals(numbers, a.getNumbers());
   }

   @Test void testGetSpecSymbols() {
      String specSymbols = "!@#$%&*()_+-=[]|,./?><";
      Symbols a = new Symbols();
      assertEquals(specSymbols, a.getSpecSymbols());
   }

   @Test void testSetSpecSymbols() {
      String specSymbols = "!@#$%&*()_+-=[]|,./?><----";
      Symbols a = new Symbols();
      a.setSpecSymbols("!@#$%&*()_+-=[]|,./?><----");
      assertEquals(specSymbols, a.getSpecSymbols());
   }
}
