package PasswordGenerator;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TestPassSettings{
   @Test void testGetSmallazTrue() {
      boolean smallaz = true;
      PassSettings def = new PassSettings();
      assertEquals(smallaz, def.getSmallaz());
   }

   @Test void testSetSmallazFalse() {
      PassSettings def = new PassSettings();
      boolean f = false;
      def.setSmallaz(f);
      assertEquals(f, def.getSmallaz());
   }

   // ---------------------------------------------------

   @Test void testGetBigAZTrue() {
      boolean bigAZ = true;
      PassSettings def = new PassSettings();
      assertEquals(bigAZ, def.getBigAZ());
   }

   @Test void testGetBigAZFalse() {
      PassSettings def = new PassSettings();
      boolean f = false;
      def.setBigAZ(f);
      assertEquals(f, def.getBigAZ());
   }

   // ---------------------------------------------------

   @Test void testGetNumberTrue() {
      boolean t = true;
      PassSettings def = new PassSettings();
      def.setNumber(t);
      assertEquals(t, def.getNumber());
   }

   @Test void testGetNumberFalse() {
      PassSettings def = new PassSettings();
      boolean f = false;
      assertEquals(f, def.getNumber());
   }

   // ---------------------------------------------------

   @Test void testGetSpecTrue() {
      boolean t = true;
      PassSettings def = new PassSettings();
      def.setSpec(t);
      assertEquals(t, def.getSpec());
   }

   @Test void testGetSpecFalse() {
      PassSettings def = new PassSettings();
      boolean f = false;
      assertEquals(f, def.getSpec());
   }

   // ---------------------------------------------------

   @Test void testGetPassSize() {
      PassSettings def = new PassSettings();
      int number = 8;
      assertEquals(number, def.getPassSize());
   }

   @Test void testGetPassSizeV2() {
      PassSettings def = new PassSettings();
      int number = 12;
      def.setPassSize(number);
      assertEquals(number, def.getPassSize());
   }
}
